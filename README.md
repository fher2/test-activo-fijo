<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

Technique test for [Athena Bitcoin](https://athenabitcoin.com/es/el-salvador-es/) in [Nest](https://github.com/nestjs/nest).

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## First part

```sql
#query 1
insert into activos_fijos (codigo, tipo_activo_id, descripcion) values ('LP0001',4,'Laptop Lenovo T415');

#query 2
select * from activos_fijos af where codigo = 'LP0001';

#query 3
select 
	art.nombre as 'Nombre area de trabajo',
	p.nombres as 'Nombre persona',
	p.n_carnet as 'Carnet persona',
	af.codigo as 'Codigo activo fijo',
	af.descripcion as 'Descripcion activo fijo',
	ta.nombre as 'Tipo de activo fijo'
from asignaciones a 
left join personas p on a.personas_id = p.id_persona 
left join areas_trabajo art on p.areas_trabajo_id = art.id_areas_trabajo 
left join activos_fijos af on a.activos_fijos_id = af.id_activo_fijo 
left join tipo_activo ta on ta.id_tipo_activo = af.tipo_activo_id 

#query 4
insert into asignaciones (personas_id, activos_fijos_id) values (1,61);

insert into historial_asignaciones (fecha_asignacion,personas_id,activos_fijos_id)
select now() ,a.personas_id, a.activos_fijos_id from asignaciones a where a.id_asignaciones = 43;

#query 5
delete from asignaciones
where asignaciones.personas_id = 1 and asignaciones.activos_fijos_id = 61;
```

## Authors

- [@FherClimaco](https://github.com/FherEnrique)

:green_heart: I love back-end
