import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ActivoFijoModule } from './activo-fijo/activo-fijo.module';
import { AsignacionesModule } from './asignaciones/asignaciones.module';
import { PrismaModule } from './prisma/prisma.module';

@Module({
  imports: [ConfigModule.forRoot(), ActivoFijoModule, AsignacionesModule, PrismaModule],
  controllers: [],
  providers: [],
})
export class AppModule { }
