import { Controller, Get, Post, Body, Patch, Param, Delete, Query, DefaultValuePipe, ParseIntPipe } from '@nestjs/common';
import { AsignacionesService } from './asignaciones.service';
import { CreateAsignacioneDto } from './dto/create-asignacione.dto';
import { asignaciones } from '@prisma/client';

@Controller('api/asignaciones')
export class AsignacionesController {
  constructor(private readonly asignacionesService: AsignacionesService) { }

  @Post()
  create(@Body() createAsignacioneDto: CreateAsignacioneDto): Promise<asignaciones> {
    return this.asignacionesService.create(createAsignacioneDto);
  }

  @Get()
  findAll(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('per_page', new DefaultValuePipe(5), ParseIntPipe) perPage: number,
  ) {
    return this.asignacionesService.findAll(page, perPage);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.asignacionesService.remove(+id);
  }
}
