import { BadRequestException, ConflictException, Injectable } from '@nestjs/common';
import { CreateAsignacioneDto } from './dto/create-asignacione.dto';
import { UpdateAsignacioneDto } from './dto/update-asignacione.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { asignaciones } from '@prisma/client';

@Injectable()
export class AsignacionesService {
  constructor(private prismaService: PrismaService) { }

  async create(createAsignacioneDto: CreateAsignacioneDto): Promise<asignaciones> {
    let newAsignacion;

    const validateAsignacion = await this.prismaService.asignaciones.findFirst({
      where: { activos_fijos_id: createAsignacioneDto.activos_fijos_id },
    });

    if (validateAsignacion) {
      throw new ConflictException('El activo fijo ya ha sido asignado.');
    }

    try {
      newAsignacion = await this.prismaService.asignaciones.create({
        data: {
          personas_id: createAsignacioneDto.personas_id,
          activos_fijos_id: createAsignacioneDto.activos_fijos_id,
        },
        include: {
          personas: true,
          activos_fijos: true,
        }
      });

      await this.prismaService.historial_asignaciones.create({
        data: {
          personas_id: createAsignacioneDto.personas_id,
          activos_fijos_id: createAsignacioneDto.activos_fijos_id,
        }
      });
    } catch (error) {
      throw new BadRequestException('No pudo realizar la asignacion de activos.');
    }

    return newAsignacion;
  }

  findAll(page: number, perPage: number) {
    const skip = (page - 1) * perPage;
    const take = perPage;

    const allAsignaciones = this.prismaService.asignaciones.findMany({
      skip: skip,
      take: take,
      include: {
        personas: {
          include: {
            areas_trabajo: true,
          }
        },
        activos_fijos: {
          include: {
            tipo_activo: true,
          }
        },
      }
    });

    return allAsignaciones;
  }

  async remove(id: number) {
    try {
      await this.prismaService.asignaciones.delete({
        where: { id_asignaciones: id }
      });
    } catch (error) {
      throw new BadRequestException('No se pudo eliminar la asignacion de activos');
    }

    return {
      'message': 'La asignacion fue eliminada',
    };
  }
}
