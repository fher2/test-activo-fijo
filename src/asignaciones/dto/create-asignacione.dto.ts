import { IsInt, IsPositive } from "class-validator";

export class CreateAsignacioneDto {
    @IsInt()
    @IsPositive()
    personas_id;

    @IsInt()
    @IsPositive()
    activos_fijos_id;
}
