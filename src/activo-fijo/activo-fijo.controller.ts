import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ActivoFijoService } from './activo-fijo.service';
import { CreateActivoFijoDto } from './dto/create-activo-fijo.dto';
import { activos_fijos } from '@prisma/client';

@Controller('api/activo-fijo')
export class ActivoFijoController {
  constructor(private readonly activoFijoService: ActivoFijoService) { }

  @Post()
  create(@Body() createActivoFijoDto: CreateActivoFijoDto): Promise<activos_fijos> {
    return this.activoFijoService.create(createActivoFijoDto);
  }

  @Get(':codeActivoFijo')
  findOne(@Param('codeActivoFijo') codeActivoFijo: string): Promise<activos_fijos> {
    return this.activoFijoService.findOne(codeActivoFijo);
  }
}
