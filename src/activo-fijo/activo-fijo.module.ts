import { Module } from '@nestjs/common';
import { ActivoFijoService } from './activo-fijo.service';
import { ActivoFijoController } from './activo-fijo.controller';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
  imports: [PrismaModule],
  controllers: [ActivoFijoController],
  providers: [ActivoFijoService],
})
export class ActivoFijoModule { }
