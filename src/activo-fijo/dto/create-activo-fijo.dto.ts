import { IsInt, IsPositive, IsString, Max } from "class-validator";

export class CreateActivoFijoDto {
    @IsString()
    @Max(12)
    codigo: string;

    @IsInt()
    @IsPositive()
    tipo_activo_id: number;

    @IsString()
    descripcion: string;
}
