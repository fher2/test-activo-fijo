import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateActivoFijoDto } from './dto/create-activo-fijo.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { activos_fijos } from '@prisma/client';

@Injectable()
export class ActivoFijoService {
  constructor(private prismaService: PrismaService) { }

  async create(data: CreateActivoFijoDto): Promise<activos_fijos> {
    let newActivoFijo: activos_fijos;

    try {
      newActivoFijo = await this.prismaService.activos_fijos.create({
        data: {
          codigo: data.codigo,
          tipo_activo_id: data.tipo_activo_id,
          descripcion: data.descripcion
        },
      });
    } catch (error) {
      throw new BadRequestException('Error al crear un activo fijo.');
    }

    return newActivoFijo;
  }

  async findOne(codigo: string): Promise<activos_fijos> {
    const tipoActivo = await this.prismaService.activos_fijos.findFirst({
      where: { codigo },
    });

    if (!tipoActivo) {
      throw new NotFoundException('No se encontro el codigo del activo fijo');
    }

    return tipoActivo;
  }
}
